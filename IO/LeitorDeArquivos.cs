using System.IO;
using static System.Console;
namespace IOLesson
{
    public class LeitorDeArquivos
    {
        private string EnderecoArquivo { get; set; }
        public LeitorDeArquivos(string enderecoArquivo)
        {
            this.EnderecoArquivo = enderecoArquivo;

        }

        public void LerArquivo()
        {
            Stream stream = File.Open(EnderecoArquivo, FileMode.Open);
            StreamReader leitor = new StreamReader(stream);
            string linha = "";
            while (leitor.Peek() != -1)
            {
                linha = leitor.ReadLine();
                WriteLine(linha);
            }
            leitor.Close();
            stream.Close();
        }

        public void LerArquivoComBoaPratica()
        {
            string linha = "";
            using(var stream = File.OpenRead(EnderecoArquivo))
            {
                using(var leitor = new StreamReader(stream))
                {
                    while (leitor.Peek() > -1)
                    {
                        linha = leitor.ReadLine();
                        WriteLine(linha);

                    }
                }
            }
        }

    }
}