using System.IO;

namespace IOLesson
{
    public class Escritor
    {
        private string EnderecoArquivo { get; set; }
        public Escritor(string enderecoArquivo)
        {
            EnderecoArquivo = enderecoArquivo;
        }

        public void EscreverArquivo(string conteudo)
        {
            Stream arquivo = File.Open(EnderecoArquivo, FileMode.OpenOrCreate);
            StreamWriter caneta = new StreamWriter(arquivo);
            caneta.WriteLine(conteudo);
            caneta.Close();
            arquivo.Close();
        }

        public void EscreverArquivoComBoaPratica(string conteudo)
        {
            using(var arquivo = File.OpenWrite(EnderecoArquivo))
            {
                using(var caneta = new StreamWriter(arquivo))
                {
                    caneta.WriteLine(conteudo);
                }
            }
        }

    }
}